﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkulator_BMR
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Waga_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if ((Waga.Text != "")&& (Wzrost.Text != "")&& (Wiek.Text != ""))
            {
                label5.ForeColor = Color.Black;
                double waga = Convert.ToDouble(Waga.Text);
                double wzrost = Convert.ToDouble(Wzrost.Text);
                double wiek = Convert.ToDouble(Wiek.Text);
                double bmr;
                double w = 9.99 * waga;
                double wz = 6.25 * wzrost;
                double wi = 4.92 * wiek;


                if (checkBox1.Checked == true)
                {
                    bmr = w + wz - wi + 5;
                    label5.Text = bmr.ToString();
                }



                if (checkBox2.Checked == true)
                {
                    bmr = w + wz - wi - 161;
                    label5.Text = bmr.ToString();
                }






            }
            else
            {
                label5.ForeColor = Color.Red;
                label5.Text = "brak danych";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Wzrost.Text = String.Empty;
            Waga.Text = String.Empty;
            Wiek.Text = String.Empty;
            checkBox1.Checked = false;
            checkBox2.Checked = false;
            label5.Text = String.Empty;


        }
    }
}
